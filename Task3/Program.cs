﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task3
{
    internal class Program
    {
        /// <summary>
        /// Игра "Бактерии"
        /// У бактерии есть одна характеристика - вес (жизни)
        /// Бактерия может есть соседей, забирая их вес
        /// Бактерия может переместиться на одну клетку в случайном направлении
        /// В конце каждого своего жизненного цикла бактерия теряет вес в зависимости от своего текущего
        /// Каждый цикл создаются новые бактерии
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            Console.WriteLine("Input number of rows in pool:");
            int rows = int.Parse(Console.ReadLine());

            Console.WriteLine("Input number of cols in pool:");
            int cols = int.Parse(Console.ReadLine());

            Game game = new Game(rows, cols);
            game.draw("Start position");
            System.Threading.Thread.Sleep(500);

            while (true)
            {                                               
                game.eatCycle();                
                game.draw("Eat cycle");
                System.Threading.Thread.Sleep(500);

                game.moveCycle();
                game.draw("Move cycle");
                System.Threading.Thread.Sleep(500);

                game.lossWeightCycle();
                game.draw("Loss weight cycle");
                System.Threading.Thread.Sleep(500);

                game.generateNewBacteriums();
                game.draw("Generate new bacteriums");
                System.Threading.Thread.Sleep(500);
            }
        }
    }

    public class Game
    {
        private const int MAX_MOVE_TRIES = 10;
        private const int BACTERIUM_GENERATION_MAX_PERCENT = 20;
        private const int NEW_BACTERIUM_MAX_LIFE = 5;
        private const int LOSS_WEIGHT_FACTOR = 5;

        private int rows;
        private int cols;
        private int[,] pool;
        private Random random;

        public Game(int iRows, int iCols)
        {
            random = new Random();

            rows = iRows;
            cols = iCols;
            pool = new int[rows, cols];
            generatePool();
        }

        /// <summary>
        /// Бактерии могут есть соседей
        /// </summary>
        public void eatCycle()
        {
            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < cols; j++)
                {
                    //Если место занято бактерией, то она пробует съесть соседей
                    if (pool[i, j] != 0)
                    {
                        tryToEatNeighbors(i, j);
                    }
                }
            }
        }

        /// <summary>
        /// Бактерии могут перемещаться
        /// </summary>
        public void moveCycle()
        {
            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < cols; j++)
                {
                    //Если место занято бактерией, то она пробует переместиться
                    if (pool[i, j] != 0)
                    {
                        tryToMove(i, j);
                    }
                }
            }
        }

        /// <summary>
        /// Бактерии теряют вес по чуть-чуть, чем больше у нее вес, тем больше теряет
        /// </summary>
        public void lossWeightCycle()
        {
            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < cols; j++)
                {
                    //Если место занято бактерией, то она пробует переместиться
                    if (pool[i, j] != 0)
                    {
                        lossWeight(i, j);
                    }
                }
            }
        }

        /// <summary>
        /// Генерируем новые бактерии в бассейне
        /// Новую бактерию можно поместить только на свободное место        
        /// </summary>
        public void generateNewBacteriums()
        {
            //Нас интересует генерация лишь приблизительного количества бактерий, ориентируемся на константу
            int maxTries = rows * cols * BACTERIUM_GENERATION_MAX_PERCENT / 100;
            int countTries = 0;
            while (countTries < maxTries)
            {
                int row = random.Next(rows);
                int col = random.Next(cols);

                //Если место свободно, то добавляем новую бактерию, с учетом ограничения на макс. кол-во жизней
                if (pool[row, col] == 0)
                {
                    createBacterium(row, col);
                }
               
                countTries++;
            }
        }

        /// <summary>
        /// Бактерия пробует есть соседей
        /// Если она является самой "жирной" то она съедает всех бактерий вокруг и получает их жизни        
        /// </summary>
        /// <param name="row"></param>
        /// <param name="col"></param>
        public void tryToEatNeighbors(int row, int col)
        {
            //Находим бактерию с максимальным весом вокруг нас
            //Если у нее такой же вес, ну что поделать, ей не повезло, мы ее съедим первыми
            int max = pool[row, col];
            //Можно вставить дополнительную проверку, когда i и j равны 0, но это не повлияет на результат
            for (int i = -1; i <= 1; i++)
            {
                for (int j = -1; j <= 1; j++)
                {
                    int iCurr = row + i;
                    int jCurr = col + j;
                    //Проверяем не вышли ли мы за границы диапазона
                    if (iCurr >= 0 && iCurr < rows && jCurr >= 0 && jCurr < cols)
                    {
                        if (pool[iCurr, jCurr] > max)
                        {
                            max = pool[iCurr, jCurr];
                        }
                    }
                }
            }

            if (max == pool[row, col])
            {
                //А здесь дополнительная проверка, когда i и j равны 0 нужна, т.к. мы не собираемся есть сами себя
                for (int i = -1; i <= 1; i++)
                {
                    for (int j = -1; j <= 1; j++)
                    {
                        if (i != 0 && j != 0)
                        {
                            int iCurr = row + i;
                            int jCurr = col + j;
                            if (iCurr >= 0 && iCurr < rows && jCurr >= 0 && jCurr < cols)
                            {
                                pool[row, col] += pool[iCurr, jCurr];
                                pool[iCurr, jCurr] = 0;
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Попытка перемещения бактерии на свободное место        
        /// </summary>
        /// <param name="row"></param>
        /// <param name="col"></param>
        public void tryToMove(int row, int col)
        {
            //Ограничиваем число попыток перемещения, если не смогли переместиться, ну что поделать, остаемся на месте
            bool isMoved = false;
            int countTries = 0;
            while (!isMoved && countTries < MAX_MOVE_TRIES)
            {
                int newRow = row + random.Next(-1, 2);
                int newCol = col + random.Next(-1, 2);

                if (newRow >= 0 && newRow < rows && newCol >= 0 && newCol < cols && pool[newRow, newCol] == 0)
                {
                    pool[newRow, newCol] = pool[row, col];
                    pool[row, col] = 0;
                    isMoved = true;
                }

                countTries++;
            }
        }

        /// <summary>
        /// Потеря веса бактерией, чем у нее больше веса, тем больше она теряет
        /// </summary>
        /// <param name="row"></param>
        /// <param name="col"></param>
        public void lossWeight(int row, int col)
        {            
            pool[row, col] -= pool[row, col] / LOSS_WEIGHT_FACTOR;
        }

        /// <summary>
        /// Отрисовка
        /// </summary>
        /// <param name="caption"></param>
        public void draw(string caption)
        {
            Console.Clear();
            Console.WriteLine(caption);
            Console.WriteLine();

            for (int i = 0; i < rows; i++)
            {
                Console.Write("|");
                for (int j = 0; j < cols; j++)
                {                    
                    if (pool[i, j] == 0)
                    {
                        Console.Write($"{' ',4}");
                    }
                    else
                    {
                        Console.Write($"{pool[i, j],4}");
                    }
                }
                Console.Write($"{'|', 4}");
                Console.WriteLine();
            }
        }

        /// <summary>
        /// Генерируем поле
        /// </summary>
        private void generatePool()
        {
            pool = new int[rows, cols];

            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < cols; j++)
                {
                    createBacterium(i, j);
                }
            }
        }

        /// <summary>
        /// Создаем новую бактерию
        /// </summary>
        /// <param name="row"></param>
        /// <param name="col"></param>
        private void createBacterium(int row, int col)
        {
            pool[row, col] = random.Next(NEW_BACTERIUM_MAX_LIFE + 1);
        }
    }
}
