﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Task2
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Input number of rows:");
            int rows = int.Parse(Console.ReadLine());

            Console.WriteLine("Input number of cols:");
            int cols = int.Parse(Console.ReadLine());

            int[][,] matrixArray = new int[3][,];
            for (int m = 0; m < matrixArray.Length; m++)
            {
                matrixArray[m] = new int[rows, cols];
            }
            
            Random random = new Random();          
            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < cols; j++)
                {                                      
                    matrixArray[0][i, j] = random.Next(100);
                    matrixArray[1][i, j] = random.Next(100);
                    matrixArray[2][i, j] = matrixArray[0][i, j] + matrixArray[1][i, j];                    
                }                
            }
            
            for (int m = 0; m < matrixArray.Length; m++)
            {
                Console.WriteLine($"\nMatrix {m + 1}:");
                for (int i = 0; i < rows; i++)
                {
                    for (int j = 0; j < cols; j++)
                    {
                        Console.Write($"{matrixArray[m][i, j],4}");                        
                    }
                    Console.WriteLine();
                }
            }
          
            Console.ReadKey(true);
        }
    }
}
