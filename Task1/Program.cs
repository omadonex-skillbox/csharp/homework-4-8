﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task1
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Input number of rows:");
            int rows = int.Parse(Console.ReadLine());

            Console.WriteLine("Input number of cols:");
            int cols = int.Parse(Console.ReadLine());

            int[,] matrix = new int[rows, cols];

            Random random = new Random();

            int sum = 0;
            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < cols; j++)
                {
                    int element = random.Next(100);
                    sum += element;
                    matrix[i, j] = element;
                    Console.Write($"{element,4}");
                    //Задание сомнительное, можно и без матрицы обойтись )
                }
                Console.WriteLine();
            }

            Console.WriteLine($"Sum of elements: {sum}");
            Console.ReadKey(true);
        }
    }
}
